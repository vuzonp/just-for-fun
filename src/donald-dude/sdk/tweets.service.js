/** Client of Twitter API. */
class TweetsService {

  #endpoint = process.env.API_ENDPOINT;
  #nextToken = null;
  #oldestId = null;

  /** Finds the lasts tweets of Donald Trump */
  findAny() {
    let endpoint = !!this.#nextToken
      ? `${this.#endpoint}?next=${this.#nextToken}`
      : this.#endpoint;

    endpoint = !!this.#oldestId
      ? `${endpoint}&oldest_id=${this.#oldestId}`
      : endpoint;

    return fetch(endpoint, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then((res) => res.json())
    .then(tweets => {
      return new Promise((resolve, reject) => {
        this.#nextToken = tweets.meta.next_token;
        this.#oldestId = tweets.meta.oldest_id
        resolve(tweets.data);
      });
    })
  }

}

module.exports = TweetsService;
