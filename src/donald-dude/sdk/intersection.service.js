/** Wrapper Service of IntersectionObserver */
class IntersectionService {

  #intersectionObserver;
  #elements = new WeakSet();
  #callbacks = [];

  constructor() {
    this.#intersectionObserver = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.intersectionRatio <= 0) return true;
        // When event is triggered, runs the callback functions.
        this.#callbacks.forEach((cb) => cb(entry, entries));
      });
    });
  }

  /** On interaction event, runs callback */
  on(callback) {
    this.#callbacks.push(callback);
  }

  /** Listens interaction from an element.  */
  attachTrigger(elem) {
    this.#elements.add(elem);
    this.#intersectionObserver.observe(elem);
  }

  /** Stop to listen interaction from an element. */
  detachTrigger(elem) {
    if (this.#elements.has(elem)) {
      this.#intersectionObserver.unobserve(elem);
      this.#elements.delete(elem);
    }
  }

  /** Stop to listen all interactions from elements. */
  revokeAll() {
    Array.from(this.#elements).forEach((elem) => {
      this.detachTriggerElement(elem)
    });
  }

}

module.exports = IntersectionService;
