import TweetsService from './sdk/tweets.service';
import IntersectionService from './sdk/intersection.service';

const intersectionService = new IntersectionService();
const tweetsService = new TweetsService();

const tweetsHandler = () => {
  const elemTarget = document.getElementById('tweets');
  const tpl = document.getElementById('tpl-message');
  const elemLogger = document.getElementById('logger');

  return tweetsService.findAny()
    .then((tweets) => {

      // Displays the tweets
      tweets.forEach((tweet) => {
        const message = tpl.cloneNode(true);
        message.querySelector('.card-body').textContent = tweet.text;
        message.id = 'tweet-' + tweet.id;
        elemTarget.appendChild(message);
      });

      // Infinite scroll
      const observableElem = document.querySelector('#tweets .card:last-of-type');
      if (observableElem) {
        intersectionService.revokeAll();
        intersectionService.attachTrigger(observableElem);
      }

    }).catch((err) => {

      // Handles error
      elemTarget.innerHTML = ''; // cleans the elem

      elemLogger.textContent = 'Impossible de récupérer les tweets. Le serveur est-il démarré ?';
      elemLogger.classList.remove('d-none');
      console.error(err);

    });
};

// Onload
window.addEventListener('load', () => {

  // First loading
  tweetsHandler().finally(() => {
    const loader = document.getElementById('donald-is-waiting');
    if (loader) {
      loader.parentElement.removeChild(loader);
    }
  });

  // Infinite scroll
  intersectionService.on(() => {
    tweetsHandler();
  });
});



