const countTilesPerimeter = require('./cube-surface-area');

it('calculates the surface area of a cube', () => {
  expect(countTilesPerimeter(2)).toBe(8);
  expect(countTilesPerimeter(1)).toBe(1);
  expect(countTilesPerimeter(3)).toBe(20);
  expect(countTilesPerimeter(4)).toBe(32);
  expect(countTilesPerimeter(5)).toBe(44);
});
