const sequence = (rows) => {
  let counter = 0;
  const output = [];
  for(let i = 1; i < rows + 1; i++) {
    let row = [];
    for(let n = 0; n < i; n++) {
      row.push(++counter);
    }
    output.push(row);
  }
  return output;
}

module.exports = sequence;
