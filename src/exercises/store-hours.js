/** Converts hour to number */
function hourToFloat(hour) {
  return parseFloat(hour.replace(':', '.'));
}

/** Shop Store hours */
class StoreHours {

  openingHours = {
    monday: [],
    tuesday: [],
    wednesday: [],
    thursday: [],
    friday: [],
    saturday: [],
    sunday: []
  };

  /**
   * Adds new hours
   * @param {*} daysSelected
   * @param {*} hoursToAdd
   */
  add(daysSelected, hoursToAdd) {
    Object.keys(daysSelected)
      .filter((day) => daysSelected[day])
      .forEach((day) => {
        const openingHours = this.openingHours[day];
        const b1 = hourToFloat(hoursToAdd.begin);
        const e1 = hourToFloat(hoursToAdd.end);

        // Checks if new hours overlying old hours.
        const overlying = openingHours.find((h) => {
          const b2 = hourToFloat(h.begin);
          const e2 = hourToFloat(h.end);
          return (
               (b1 > b2 && b1 < e2)
            || (e1 > b2 && e1 < e2)
            || (b2 > b1 && b2 < e1)
            || (e2 > b1 && e2 < e1)
          );
        });

        if (!overlying) {
          this.openingHours[day] = [
            ...openingHours,
            hoursToAdd
          ].sort((a, c) => hourToFloat(a.begin) - hourToFloat(c.begin));
        }
      })
  }

  /** Gets all opening hours */
  getAll() {
    return this.openingHours;
  }

}

module.exports = StoreHours;
