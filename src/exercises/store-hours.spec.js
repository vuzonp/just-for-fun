const StoreHours = require('./store-hours');

it('can add a new opening hours', () => {
  const hours = { begin: '19:00', end: '23:00' };
  const expected = {
    monday: [hours],
    tuesday: [hours],
    wednesday: [hours],
    thursday: [hours],
    friday: [],
    saturday: [],
    sunday: [],
  }

  const oHours = new StoreHours();
  oHours.add({
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: false,
    saturday: false,
    sunday: false,
  },
  {
    begin: '19:00',
    end: '23:00'
  });

  expect(oHours.getAll()).toMatchObject(expected);
});

it('returns opening hours in chronological order.', () => {
  const oHours = new StoreHours();
  oHours.add({
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: false,
    saturday: false,
    sunday: false,
  },
  {
    begin: '19:00',
    end: '23:00'
  });

  oHours.add({
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: false,
    saturday: false,
    sunday: false,
  },
  {
    begin: '11:30',
    end: '14:00'
  });

  oHours.add({
    monday: true,
    tuesday: true,
    wednesday: true,
    thursday: true,
    friday: false,
    saturday: false,
    sunday: false,
  },
  {
    begin: '6:00',
    end: '9:00'
  });

  expect(oHours.getAll().monday[0]).toMatchObject({begin: '6:00', end: '9:00'});
  expect(oHours.getAll().monday[1]).toMatchObject({begin: '11:30', end: '14:00'});
  expect(oHours.getAll().monday[2]).toMatchObject({begin: '19:00', end: '23:00'});

});

it('should not allow to recover a existing hour', () => {
  const oHours = new StoreHours();
  oHours.add({ monday: true }, {
    begin: '19:00',
    end: '23:00'
  });
  oHours.add({ monday: true }, {
    begin: '18:00',
    end: '20:00'
  });
  expect(oHours.getAll().monday.length).toBe(1);

  oHours.add({ tuesday: true }, {
    begin: '8:00',
    end: '12:00'
  });
  oHours.add({ tuesday: true }, {
    begin: '9:00',
    end: '10:00'
  });
  expect(oHours.getAll().tuesday.length).toBe(1);

  oHours.add({ wednesday: true }, {
    begin: '9:00',
    end: '10:00'
  });
  oHours.add({ wednesday: true }, {
    begin: '8:00',
    end: '12:00'
  });
  expect(oHours.getAll().wednesday.length).toBe(1);

  oHours.add({ thursday: true }, {
    begin: '8:00',
    end: '10:00'
  });
  oHours.add({ thursday: true }, {
    begin: '10:00',
    end: '12:00'
  });
  expect(oHours.getAll().thursday.length).toBe(2);

})
