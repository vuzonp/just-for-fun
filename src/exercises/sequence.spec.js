const sequence = require('./sequence');

it('the first row must contain an array of 1 value which is worth 1', () => {
  expect(sequence(1)[0]).toStrictEqual([1]);
  expect(sequence(10)[0]).toStrictEqual([1]);
});

it('each row must have the same length than is offset + 1 position in parent', () => {
  const seq = sequence(23);
  for (let i = 0; i < seq.length; i++ ) {
    expect(seq[i].length).toStrictEqual(i + 1);
  }
});

it('must increment the starting value in each entry', () => {
  const seq = sequence(5).flat();
  for (let i = 0; i < seq.length; i++ ) {
    expect(seq[i]).toStrictEqual(i + 1);
  }

});
