/**
 * calculates the number of miniature cubes that make up
 * the perimeter of a cube made from them.
 * @param {number} edgeTiles The number of small cube used for make an edge.
 */
module.exports = function countTilesPerimeter(edgeTiles) {
  if (isNaN(edgeTiles) || edgeTiles < 1) return 0;
  if (edgeTiles === 1) return 1;
  return (edgeTiles - 1) * 4 * 2 + (edgeTiles - 2) * 4;
}
