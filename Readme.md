# Exercices de développement à destination de Placeloop

Ce dépôt concentre mes réponses apportées aux tests techniques soumis par Placeloop.

## Excercices algorithmiques

Les exercices algorithmiques sont placés dans le dossier `src/exercises`.

Pour vérifier la validité des réponses : `npm test`

## Page web affichant les tweets de Donald Trump

Il vous faut utiliser une API proxy pour récupérer les tweets et contourner les problèmes des CORS : https://gitlab.com/vuzonp/donald-dude-server

Pour afficher la page : `npm start`


## Roadmap
Liste non exhaustive d'évolutions envisagées.

- [x] Réorganiser le code source pour plus de clarté (séparer les test algo du site).
- [x] Utiliser l'API Twitter pour prendre le contrôle du format des tweets.
- [ ] Utiliser un moteur de template html pour le site.
- [ ] Déployer le site en ligne.
- [x] Apporter une touche d'humour à la page web
- [ ] Personnaliser les styles de la page
- [ ] Ajouter des microdata.
